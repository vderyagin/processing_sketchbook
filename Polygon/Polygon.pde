void setup() {
  size(400, 400);
  background(255);
  fill(100);
  translate(width/2, height/2);
  rotate(PI/7);
  polygon(6, 100.0);
}

void polygon(int sideCount, float radius) {
  beginShape();

  for (float theta = 0.0; theta <= TWO_PI; theta += TWO_PI / sideCount) {
    vertex(cos(theta) * radius, sin(theta) * radius);
  }

  endShape(CLOSE);
}
