class ViewPort {
  float x1, y1, x2, y2;
  float zoomFactor = 1.2;

  ViewPort(float x1, float y1, float x2, float y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  ViewPort() {
    this(-3.0, -1.3, 1.3, 1.3);
  }

  PVector realCoords(int x, int y) {
    return new PVector(map(x, 0, width, this.x1, this.x2),
                       map(y, 0, height, this.y1, this.y2));
  }

  PVector realCoords(int pixelIdx) {
    int y = height - pixelIdx / width;
    int x = pixelIdx % width;
    return this.realCoords(x, y);
  }

  void zoomIn(int x, int y) {
    PVector coords = realCoords(x, y);

    this.x1 = x1 + (coords.x - x1) / zoomFactor;
    this.y1 = y1 + (coords.y - y1) / zoomFactor;
    this.x2 = x2 - (x2 - coords.x) / zoomFactor;
    this.y2 = y2 - (y2 - coords.y) / zoomFactor;

    draw();
  }

  void zoomOut(int x, int y) {

    if (x2 - x1 < 5) {
      PVector coords = realCoords(x, y);

      this.x1 = x1 - (coords.x - x1) * zoomFactor;
      this.y1 = y1 - (coords.y - y1) * zoomFactor;
      this.x2 = x2 + (x2 - coords.x) * zoomFactor;
      this.y2 = y2 + (y2 - coords.y) * zoomFactor;
    }

    draw();
  }

  void drag(int x, int y) {
    float horizontalOffset = (x2 - x1) / width * x;
    float verticalOffset = (y2 - y1) / height * y;

    x1 -= horizontalOffset;
    x2 -= horizontalOffset;
    y1 += verticalOffset;
    y2 += verticalOffset;

    draw();
  }

  void draw() {
    int minIter = minIterations();

    loadPixels();
    for (int i = 0; i < pixels.length; i++) {
      int iter = iterationsToConverge(realCoords(i));
      color c = iter == max_iterations ? color(0) : color(map(iter, minIter, max_iterations, 0, 1000.0), 1000, 1000);
      pixels[i] = c;
    }
    updatePixels();
  }

  int minIterations() {
    int minIter = max_iterations;

    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        int iter = iterationsToConverge(realCoords(i, i));
        if (iter < minIter) {
          minIter = iter;
        }
      }
    }

    return minIter;
  }
}
