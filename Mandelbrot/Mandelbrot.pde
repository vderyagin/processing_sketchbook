int max_iterations = 1000;
ViewPort vp = new ViewPort();

int dragStartMouseX, dragStartMouseY;

void setup() {
  fullScreen();
  background(255);
  colorMode(HSB, 1000.0);

  vp.draw();
}


void draw() {
}

int iterationsToConverge(PVector p) {
  float x = 0.0, y = 0.0;
  float xtemp;
  int iteration = 0;

  while (x * x + y * y < 4 && iteration < max_iterations) {
    xtemp = x * x - y * y + p.x;
    y = 2 * x * y + p.y;
    x = xtemp;
    iteration++;
  }

  return iteration;
}


void keyTyped() {
  if (key == '+') {
    vp.zoomIn(mouseX, mouseY);
  } else if (key == '-') {
    vp.zoomOut(mouseX, mouseY);
  }
}

void mousePressed() {
  dragStartMouseX = mouseX;
  dragStartMouseY = mouseY;
}

void mouseReleased() {
  vp.drag(mouseX - dragStartMouseX, mouseY - dragStartMouseY);
}
