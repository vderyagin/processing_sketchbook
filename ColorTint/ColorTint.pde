void setup() {
  size(1000, 750);
  PImage img = loadImage("cat.jpg");

  tint(200, 100, 0);
  image(img, 0, 0);
  tint(0, 100, 200);
  image(img, img.width, 0);
  tint(100, 100, 200);
  image(img, 0, img.height);
  tint(200, 200, 100);
  image(img, img.width, img.height);
}
