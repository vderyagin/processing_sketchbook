import processing.opengl.*;

void setup() {
  float[][] data = new float[800][800];
  size(800, 800);
  background(0);
  smooth();
  genNoise(data);
  asRotation(data);
}

void genNoise(float[][] data) {
  float noiseScale = 0.01;
  noiseDetail(2);
  for (int i = 0; i < data.length; i++) {
    for (int j = 0; j < data[i].length; j++) {
      data[i][j] = noise(i * noiseScale, j * noiseScale);
    }
  }
}

void asRotation(float[][] data) {
  int s = 5;
  for (int i = 0; i < data.length; i += s) {
    for (int j = 0; j < data[i].length; j += 2) {
      stroke(255, 50);
      strokeWeight(2);
      pushMatrix();
      translate(j, i);
      rotate(data[i][j] * TWO_PI);
      line(0, 0, s * 4, 0);
      popMatrix();
    }
  }
}
