void setup() {
  size(800, 600);
  background(0);
  noStroke();

  float
    cols = 40,
    rows = 30,
    cellW = width/cols,
    cellH = height/rows;

  for (int i = 0, k = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      pushMatrix();

      translate(cellW / 2 + cellW * j, cellH/ 2 + cellH * i);

      if (isPrime(k)) {
        primeCell(cellW, cellH);
      } else {
        compositeCell(cellW, cellH);
      }

      popMatrix();
      k++;
    }
  }
}

void primeCell(float w, float h) {
  fill(255, 0, 0);
  ellipse(0, 0, w, h);
}

void compositeCell(float w, float h) {
  fill(255);
  rect(-w / 2, -h / 2, w, h);
}

boolean isPrime(int val) {
  if (val < 2) {
    return false;
  }

  for (int i = 2; i < val; i++) {
    if (val % i == 0) {
      return false;
    }
  }

  return true;
}
