float[] price;
float minPrice, maxPrice;
float X1, Y1, X2, Y2;

PFont legendFont = createFont("SansSeriv", 20);

void setup() {
  size(600, 400);
  X1 = 50;
  Y1 = 50;
  X2 = width - 50;
  Y2 = height - Y1;
  smooth();

  textFont(legendFont);

  String[] lines = loadStrings("AAPL.csv");
  price = new float[lines.length];

  for (int i = 0; i < lines.length; i++) {
    String[] pieces = split(lines[i], ",");
    price[i] = float(pieces[4]);
  }

  minPrice = min(price);
  maxPrice = max(price);

  println("Data loaded: " + price.length + " entries.");
  println("Min: " + minPrice);
  println("Max: " + maxPrice);
}

void draw() {
  background(0);
  rectMode(CORNERS);
  noStroke();
  fill(255);
  rect(X1, Y1, X2, Y2);
  drawGraph(price, minPrice, maxPrice);

  fill(255);
  textSize(18);
  textAlign(LEFT);
  text("(AAPL) Apple Inc. 2010", X1, Y1 - 10);
  textSize(10);
  textAlign(RIGHT, BOTTOM);
  text("Source: Yahoo! Finance (finance.yahoo.com)", width - 10, height - 10);

  drawXLabels();
  drawYLabels();
  movingAverage(price, minPrice, maxPrice, 30);
}

void drawGraph(float[] data, float minVal, float maxVal) {
  stroke(0);

  beginShape();
  for (int i = 0; i < data.length; i++) {
    float x = map(i, 0, data.length - 1, X1, X2);
    float y = map(data[i], minVal, maxVal, Y2, Y1);
    vertex(x, y);
  }
  endShape();
}

void drawXLabels() {
  fill(255);
  textSize(20);
  textAlign(LEFT);

  for (int i = 1; i <= 12; i++) {
    float x = map(i, 1, 13, X1, X2);
    stroke(255);
    text(i, x, Y2 + 15);
    stroke(127);
    line(x, Y1, x, Y2);
  }

  textSize(18);
  text("Month", width / 2, Y2 + 35);
}

void drawYLabels() {
  fill(255);
  textSize(20);
  textAlign(RIGHT);
  stroke(255);

  for (float i = minPrice; i <= maxPrice; i += 10) {
    float y = map(i, minPrice, maxPrice, Y2, Y1);
    text(floor(i), X1 - 10, y);
    line(X1, y, X1 - 5, y);
  }

  textSize(18);
  text("$", X1 - 40, height / 2);
}

void movingAverage(float[] data, float minVal, float maxVal, int MAP) {
  noFill();
  stroke(255, 0, 0);
  strokeWeight(2);

  beginShape();
  for (int i = MAP - 1; i < data.length; i++) {
    float sum = 0;

    for (int k = i - (MAP - 1); k <= i; k++) {
      sum += data[k];
    }

    float
      MA = sum / MAP,
      x = map(i, 0, data.length - 1, X1, X2),
      y = map(MA, minVal, maxVal, Y2, Y1);

    vertex(x, y);
  }
  endShape();
}
