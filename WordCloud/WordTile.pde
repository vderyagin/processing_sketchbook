class WordTile extends Word {
  PVector location;
  float tileW, tileH;
  color tileColor;
  float tileFS = 24;

  WordTile(String word, int frequency) {
    super(word, frequency);
    setSize();
    location = new PVector(0, 0);
    tileColor = color(0);
  }

  void setXY(float x, float y) {
    location.x = x;
    location.y = y;
  }

  void setFontSize() {
    tileFS = map(frequency, 64, 1233, 10, 120);
    setSize();
  }

  void setSize() {
    textSize(tileFS);
    tileW = textWidth(word);
    tileH = textAscent();
  }

  void display() {
    fill(tileColor);
    textSize(tileFS);
    text(word, location.x, location.y);
  }

  boolean intersect(WordTile other) {
    float
      left1 = location.x,
      right1 = left1 + tileW,
      bot1 = location.y,
      top1 = bot1 - tileH,
      left2 = other.location.x,
      right2 = left2 + other.tileW,
      bot2 = other.location.y,
      top2 = bot2 - other.tileH;

    return !(right1 < left2 || left1 > right2 || bot1 < top2 || top1 > bot2);
  }
}
