class Word implements Comparable<Word> {
  String word;
  int frequency;

  Word (String word, int frequency) {
    this.word = word;
    this.frequency = frequency;
  }

  String toString () {
    return "<" + word + ", " + frequency + ">";
  }

  int compareTo(Word w) {
    return frequency - w.frequency;
  }
}
