PFont tnr;
int N = 150;
WordFreq table;

void setup() {
  size(800, 800);
  tnr = createFont("Times New Roman", 120);
  textFont(tnr);
  textSize(24);
  noLoop();

  table = new WordFreq("moby_dick.txt", N);
  table.tabulate();
  table.arrange();
}

void draw() {
  background(255);
  table.display();
}
