import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

class WordFreq {
  String[] tokens;
  HashSet<String> stopWords;
  WordTile[] sortedWords;

  WordFreq(String inputFile, int limit) {
    this.tokens = splitTokens(join(loadStrings(inputFile), " ").toLowerCase(),
                              " ,./?<>;:'\"[{}]\\|=+-()*&^%$#@!~");

    this.stopWords = new HashSet();

    for (String sw : loadStrings("stopwords.txt")) {
      this.stopWords.add(sw);
    }

    this.sortedWords = sortedByFrequency(limit);
  }

  boolean isStopWord(String w) {
    return this.stopWords.contains(w);
  }

  boolean isWord(String w) {
    char firstChar = w.charAt(0);
    return firstChar >= 'a' && firstChar <= 'z';
  }

  HashMap<String, Integer> wordFrequency() {
    HashMap<String, Integer> freqs = new HashMap();

    for (String t : this.tokens) {
      if (isStopWord(t) || !isWord(t)) {
        continue;
      }

      int newFrequency = 1;

      if (freqs.containsKey(t)) {
        newFrequency = freqs.get(t) + 1;
      }

      freqs.put(t, newFrequency);
    }

    return freqs;
  }

  WordTile[] sortedByFrequency(int limit) {
    ArrayList<WordTile> list = new ArrayList();
    WordTile[] mostFrequentWords = new WordTile[limit];

    for (Map.Entry<String, Integer> entry : wordFrequency().entrySet()) {
      list.add(new WordTile(entry.getKey(), entry.getValue()));
    }

    Collections.sort(list, Collections.reverseOrder());

    for (int i = 0; i < limit; i++) {
      mostFrequentWords[i] = list.get(i);
    }

    return mostFrequentWords;
  }

  void tabulate() {
    for (WordTile word : sortedWords) {
      println(word);
    }
  }

  void arrange() {
    for (WordTile word : sortedWords) {
      word.setFontSize();

      float
        cx = width / 2,
        cy = height / 2,
        px, py,
        R = 0.0,
        dR = 0.2,
        theta = 0.0,
        dTheta = 0.5;

      do {
        float x = cx + R * cos(theta);
        float y = cy + R * sin(theta);
        word.setXY(x, y);
        px = x;
        py = y;
        theta += dTheta;
        R += dR;
      } while (!clear(word));
    }
  }

  boolean clear(WordTile wt) {
    for (WordTile word : sortedWords) {
      if (word == wt) {
        break;
      }

      if (wt.intersect(word)) {
        return false;
      }
    }

    return true;
  }

  void display() {
    for (WordTile word: sortedWords) {
      word.display();
    }
  }
}
