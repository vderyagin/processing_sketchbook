void setup() {
  size(600, 600);
  background(255);
  translate(width / 2, height / 2);
  curveEllipse(4, 250, 4, -0.675);
}

void curveEllipse(int pts, float radius, float handleRadius, float tightness) {
  float theta = 0;
  float cx = 0, cy = 0;
  float ax = 0, ay = 0;
  float rot = TWO_PI / pts;

  curveTightness(tightness);

  beginShape();

  for (int i = 0; i < pts; i++) {
    if (i == 0) {
      cx = cos(theta - rot) * radius;
      cy = sin(theta - rot) * radius;
      curveVertex(cx, cy);
    }

    ax = cos(theta) * radius;
    ay = sin(theta) * radius;
    curveVertex(ax, ay);

    if (i == pts - 1) {
      cx = cos(theta + rot) * radius;
      cy = sin(theta + rot) * radius;
      ax = cos(theta + rot * 2) * radius;
      ay = sin(theta + rot * 2) * radius;
      curveVertex(cx, cy);
      curveVertex(ax, ay);
    }

    fill(255, 127, 0);
    ellipse(cos(theta) * radius, sin(theta) * radius,
            handleRadius * 2, handleRadius * 2);

    theta += rot;
  }


  fill(0, 127);
  noStroke();
  endShape();
}
