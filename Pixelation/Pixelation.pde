void setup() {
  size(500, 375);

  PImage img = loadImage("cat.jpg");

  int
    xInc = width / 50,
    yInc = height / 37;

  noStroke();

  for (int y = 0; y < img.height; y += yInc) {
    for (int x = 0; x < img.width; x += xInc) {
      fill(img.get(x, y));
      rect(x, y, xInc, yInc);
    }
  }
}
