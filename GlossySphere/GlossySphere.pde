void setup() {
  size(600, 600, P3D);
  background(30, 30, 30);
  noStroke();
}

void draw() {
  sphereDetail(170);
  ambient(250, 100, 100);
  ambientLight(40, 20, 40);
  lightSpecular(255, 215, 215);
  directionalLight(185, 195, 255, -1, 1.25, -1);
  translate(width / 2, height / 2);
  shininess(255);
  sphere(160);
}
