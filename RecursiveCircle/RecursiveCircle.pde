void setup() {
  size(800, 800);
  background(255);
  noFill();
  translate(width / 2, height / 2);
  drawCircle(0, 0, width / 2);
}

void drawCircle(int x, int y, int s) {
  if (s > 2) {
    ellipse(x, y, s, s);
    drawCircle(x - s / 2, y, s / 2);
    drawCircle(x + s / 2, y, s / 2);
  }
}
