void setup() {
  size(500, 375);

  PImage img = loadImage("cat.jpg");

  for (int y = 0; y < img.height; y++) {
    for (int x = 0; x < img.width; x++) {
      color c = img.get(x, y);
      set(x, y, color(255 - red(c),
                      255 - green(c),
                      255 - blue(c)));
    }
  }
}
