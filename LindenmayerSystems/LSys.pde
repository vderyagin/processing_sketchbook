import java.util.HashMap;

class LSys {
  String axiom;
  HashMap<Character, String> rules;
  String out;

  LSys() {
    axiom = "";
    rules = new HashMap();
    out = "";
  }

  void setAxiom(String s) {
    axiom += s;
    out += axiom;
  }

  void addRule(char c, String expansion) {
    rules.put(c, expansion);
  }

  String replacementFor(char c) {
    String expansion = rules.get(c);

    if (expansion != null) {
      return expansion;
    } else {
      return String.valueOf(c);
    }
  }

  void genString(int n) {
    if (n <= 0) {
      return;
    }

    String temp = "";

    for (char c : out.toCharArray()) {
      temp += replacementFor(c);
    }

    out = temp;
    genString(n - 1);
  }

  void render(float size, float angle) {
    for (char c : out.toCharArray()) {
      switch (c) {
        case 'F':
          line(0, 0, size, 0);
          translate(size, 0);
          break;
        case '+':
          rotate(radians(-angle));
          break;
        case '-':
          rotate(radians(angle));
          break;
        case '[':
          pushMatrix();
          break;
        case ']':
          popMatrix();
          break;
        default:
          break;
        }
    }
  }
}
