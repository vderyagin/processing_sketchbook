void setup() {
  size(800, 800);
  background(255);
  translate(0, height / 2);

  LSys kochSnowFlake = new LSys();
  kochSnowFlake.setAxiom("F++F++F");
  kochSnowFlake.addRule('F', "F-F++F-F");
  kochSnowFlake.genString(6);
  kochSnowFlake.render(2, 60);
}
