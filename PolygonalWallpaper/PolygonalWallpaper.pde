void setup() {
  fullScreen();
  background(255);
  noFill();
  int polyCount = 5000;
  int sideCount = 0;
  float radius = 0.0;
  float rotation = 0.0;

  for (int i = 0; i < polyCount; i++) {
    sideCount = int(random(3, 15));
    radius = random(2, 20);
    rotation = random(TWO_PI);
    pushMatrix();
    translate(random(width), random(height));
    rotate(rotation);
    polygon(sideCount, radius);
    popMatrix();
  }
}

void polygon(int sideCount, float radius) {
  beginShape();

  for (float theta = 0.0; theta - 0.01 <= TWO_PI; theta += TWO_PI / sideCount) {
    vertex(cos(theta) * radius, sin(theta) * radius);
  }

  endShape(CLOSE);
}
