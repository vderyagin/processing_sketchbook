PImage img;

void setup() {
  size(500, 375);
  img = loadImage("cat.jpg");
  image(img, 0, 0);
}

void draw() {
  for (int y = 1; y < img.height - 1; y++) {
    for (int x = 0; x < img.width - 1; x++) {
      int newX = randInt(x - 1, x + 1);
      int newY = randInt(y - 1, y + 1);
      set(x, y, get(newX, newY));
    }
  }
}

int randInt(int low, int high) {
  int r = floor(random(low, high + 1));
  r = constrain(r, low, high);
  return r;
}
