float[] ax, ay;
final int N = 5;

int isOnAnchor;
float radius = 5;

float curvature = 0;
float sliderBarX, sliderBarY, sliderBarW;
float sliderHandleX, sliderHandleY, sliderHandleW = 12;
float sliderMin = -3, sliderMax = 3;
boolean isOnSliderHandle;

void setup() {
  size(600, 600);
  background(255);

  ax = new float[N];
  ay = new float[N];

  for (int i = 0; i < N; i++) {
    ax[i] = random(100, width - 100);
    ay[i] = random(100, height - 100);
  }

  sliderBarW = width / 3;
  sliderBarX = width - sliderBarW - 40;
  sliderBarY = height - 40;
  sliderHandleX = sliderBarX + sliderBarW / 2;
  sliderHandleY = sliderBarY;
}

void draw() {
  background(255);
  noFill();
  strokeWeight(4);
  stroke(0);
  curveTightness(curvature);

  beginShape();
  curveVertex(ax[0], ay[0]);
  for (int i = 0; i < N; i++) {
    curveVertex(ax[i], ay[i]);
  }
  curveVertex(ax[N - 1], ay[N - 1]);
  endShape();

  strokeWeight(1);
  fill(255, 127, 0);
  for (int i = 0; i < N; i++) {
    ellipse(ax[i], ay[i], radius * 2, radius * 2);
  }

  for (int i = 0; i < N; i++) {
    if (dist(mouseX, mouseY, ax[i], ay[i]) < radius) {
      isOnAnchor = i;
    }
  }

  if (!mousePressed) {
    isOnSliderHandle = false;
    isOnAnchor = -1;
  }

  setCurvatureControlGUI();
}

void setCurvatureControlGUI() {
  line(sliderBarX, sliderBarY, sliderBarX + sliderBarW, sliderBarY);
  fill(0, 0, 255);
  rect(sliderHandleX - sliderHandleW / 2, sliderHandleY - sliderHandleW / 2,
       sliderHandleW, sliderHandleW);

  if (isOnSliderHandle && mouseX > sliderBarX && mouseX < sliderBarX + sliderBarW) {
    sliderHandleX = mouseX;
  }

  if (dist(mouseX, mouseY, sliderHandleX, sliderHandleY) < sliderHandleW / 2) {
    isOnSliderHandle = true;
  }

  curvature = map(sliderHandleX, sliderBarX, sliderBarX + sliderBarW, sliderMin, sliderMax);
}

void mouseDragged() {
  if (isOnAnchor >= 0 && isOnAnchor < N) {
    ax[isOnAnchor] = mouseX;
    ay[isOnAnchor] = mouseY;
  }
}
