ball[] balls;
stick[] sticks;

int stickLen = 80;
int nBalls = 10;
int nSticks = nBalls;

void setup() {
  size(400, 400);
  smooth();

  balls = new ball[nBalls];
  sticks = new stick[nSticks];

  for (int i = 0; i < nBalls; i++) {
    balls[i] = new ball(10, color(0));

    if (i > 0) {
      sticks[i - 1] = new stick(balls[i - 1], balls[i]);
    }
  }

  sticks[nSticks - 1] = new stick(balls[nBalls - 1], balls[0]);
}

void draw() {
  background(255);

  for (stick stick : sticks) {
    stick.update();
    stick.display();
  }
}
