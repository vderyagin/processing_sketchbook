class ball {
  color ballColor;

  PVector
    oldLocation,
    location,
    nudge;

  float radius;

  ball() {
    this(random(10, 40), color(random(255), random(255), random(255)));
  }

  ball(float r, color c) {
    this(new PVector(random(width), random(height)), r, c);
  }

  ball(PVector loc, float r, color c) {
    this.location = loc;
    this.radius = r;
    this.ballColor = c;
    this.oldLocation = location.get();
    this.nudge = new PVector(random(1, 3), random(1, 3));
    location.add(nudge);
  }

  void move() {
    PVector temp = location.get();
    location.add(PVector.sub(location, oldLocation));
    oldLocation.set(temp);

    bounce();
  }

  void display() {
    noStroke();
    fill(ballColor);
    ellipse(location.x, location.y, 2 * radius, 2 * radius);
  }

  void bounce() {
    if (location.x > (width - radius)) {
      location.x = width - radius;
      oldLocation.x = location.x;
      location.x -= nudge.x;
    }

    if (location.x < radius) {
      location.x = radius;
      oldLocation.x = location.x;
      location.x += nudge.x;
    }

    if (location.y > (height - radius)) {
      location.y = height - radius;
      oldLocation.y = location.y;
      location.y -= nudge.y;
    }

    if (location.y < radius) {
      location.y = radius;
      oldLocation.y = location.y;
      location.y += nudge.y;
    }
  }
}
