PVector[] sites;
ArrayList<Particle> ps;
int
  numSites = 10,
  numParticles = 5000;

void setup() {
  size(800, 800);
  smooth();
  colorMode(HSB, 360, 100, 100);
  sites = new PVector[numSites];
  reset();
}

void draw() {
  background(255);
  for (int i = 0; i < sites.length; i++) {
    noStroke();
    fill(200);
    ellipse(sites[i].x, sites[i].y, 20, 20);
  }

  for (int i = ps.size() - 1; i >= 0; i--) {
    Particle p = (Particle)ps.get(i);
    p.display();
    if (!p.converge() && p.update()) {
      ps.remove(i);
    }
  }
}

void reset() {
  for (int i = 0; i < sites.length; i++) {
    sites[i] = new PVector(random(width), random(height));
  }
  ps = new ArrayList();
  for (int i = 0; i < numParticles; i++) {
    ps.add(new Particle(random(width), random(height)));
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    sites[numSites - 1] = new PVector(mouseX, mouseY);
    for (Particle particle : ps) {
      particle.stable = false;
    }
  } else if (mouseButton == RIGHT) {
    reset();
  }
}
