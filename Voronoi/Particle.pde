class Particle {
  PVector center;
  float radius;
  PVector v;
  color c;
  boolean stable;

  Particle(float x, float y) {
    center = new PVector(x, y, 0);
    radius = 2;
    if (random(1) < 0.5) {
      c = color(random(170, 190), 70, random(100));
    } else {
      c = color(random(40, 60), 70, random(100));
    }

    int i = nearestSite();
    v = PVector.sub(center, sites[i]);
    v.normalize();
    stable = false;
  }

  void display() {
    fill(c);
    ellipse(center.x, center.y, radius * 2, radius * 2);
  }

  int nearestSite() {
    float minDist = PVector.dist(center, sites[0]);
    float minDist2 = PVector.dist(center, sites[1]);
    int minIdx = 0;
    int minIdx2 = 1;
    for (int i = 1; i < sites.length; i++) {
      float d = PVector.dist(center, sites[i]);
      if (d < minDist) {
        minDist2 = minDist;
        minIdx2 = minIdx;
        minDist = d;
        minIdx = i;
      } else if (d < minDist2) {
        minDist2 = d;
        minIdx2 = i;
      }
    }

    if (minDist2 - minDist < 1) {
      stable = true;
    }

    return minIdx;
  }

  boolean update() {
    center.add(v);
    int i = nearestSite();
    float d = PVector.dist(center, sites[i]);
    v = PVector.sub(center, sites[i]);
    v.normalize();
    v.mult(2);
    return (center.x > width || center.x < 0 || center.y > height || center.y < 0);
  }

  boolean converge() {
    return stable;
  }
}
