void setup() {
  size(600, 600);
  background(255);
  noSmooth();
  // plotRect(100, 100, 400, 400);
  plotRandomizedQuad(200, 200, 200, 200, 0.2, 0.2);
}

void plotRect(float x, float y, float w, float h) {
  beginShape();
  vertex(x, y);
  vertex(x, y + h);
  vertex(x + w, y + h);
  vertex(x + w, y);
  endShape(CLOSE);
}

void plotRandomizedQuad(float x, float y, float w, float h, float randW, float randH) {
  float
    jitterW = w * randW,
    jitterH = h * randH;

  beginShape();
  vertex(x + random(-jitterW, jitterW), y + random (-jitterH, jitterH));
  vertex(x + random(-jitterW, jitterW), y + h + random (-jitterH, jitterH));
  vertex(x + w + random(-jitterW, jitterW), y + h + random (-jitterH, jitterH));
  vertex(x + w + random(-jitterW, jitterW), y + random (-jitterH, jitterH));
  endShape(CLOSE);
}
