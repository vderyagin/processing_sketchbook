size(600, 800);

background(0);
stroke(255);
noFill();

ellipseMode(CORNER);

// head
float
  headHeight = 600,
  headWidth = headHeight * 5 / 7,
  head_x = (width - headWidth)/2,
  head_y = (height - headHeight)/2;

// eyes
float
  eyeWidth     = headWidth / 5,
  eyeHeight    = eyeWidth / 2,
  irisDiam     = eyeHeight,
  pupilDiam    = irisDiam / 3,
  eye_y        = head_y + headHeight / 2 - eyeHeight / 2,
// left
  leftEye_x    = head_x + eyeWidth,
  leftIris_x   = leftEye_x + eyeWidth / 2 - irisDiam / 2,
  leftPupil_x  = leftEye_x + eyeWidth / 2 - pupilDiam / 2,
// right
  rightEye_x   = head_x + eyeWidth * 3,
  rightIris_x  = rightEye_x + eyeWidth / 2 - irisDiam / 2,
  rightPupil_x = rightEye_x + eyeWidth / 2 - pupilDiam / 2;

// eyebrows
float
  eyeBrowWidth   = eyeWidth * 1.25,
  eyeBrowHeight  = eyeHeight / 4,
  eyeBrow_y      = eye_y - eyeHeight - eyeBrowHeight / 2,
  leftEyeBrow_x  = leftEye_x - (eyeBrowWidth - eyeWidth),
  rightEyeBrow_x = rightEye_x;

// nose
float
  nose_x = head_x + eyeWidth * 2,
  nose_y = head_y + headHeight - headHeight / 4;

// mouth
float
  mouthWidth  = eyeWidth * 1.5,
  mouthHeight = headHeight / 12,
  mouth_x     = leftIris_x + irisDiam / 2 + eyeWidth / 4,
  mouth_y     = nose_y + mouthHeight;

// ears
float
  earWidth   = eyeHeight * 1.5,
  earHeight  = headHeight / 4,
  ear_y      = eyeBrow_y,
  leftEar_x  = head_x - earWidth / 2,
  rightEar_x = head_x + headWidth - earWidth / 2;

ellipse(head_x, head_y, headWidth, headHeight); // head

ellipse(leftEye_x, eye_y, eyeWidth, eyeHeight); // left eye
ellipse(leftIris_x, eye_y, irisDiam, irisDiam);  // left iris
ellipse(leftPupil_x, eye_y + eyeHeight / 2 - pupilDiam / 2,
        pupilDiam, pupilDiam);  // left pupil

ellipse(rightEye_x, eye_y, eyeWidth, eyeHeight); // right eye
ellipse(rightIris_x, eye_y, irisDiam, irisDiam);  // right iris
ellipse(rightPupil_x, eye_y + eyeHeight / 2 - pupilDiam / 2,
        pupilDiam, pupilDiam);  // right pupil

rect(leftEyeBrow_x, eyeBrow_y, eyeBrowWidth, eyeBrowHeight); // left eyebrow
rect(rightEyeBrow_x, eyeBrow_y, eyeBrowWidth, eyeBrowHeight); // right eyebrow

triangle(nose_x, nose_y, nose_x + eyeWidth, nose_y,
         nose_x + eyeWidth / 2, nose_y - eyeWidth); // nose

arc(mouth_x, mouth_y - mouthHeight / 2, mouthWidth, mouthHeight,
    PI, TWO_PI);                          // top lip

arc(mouth_x, mouth_y - mouthHeight / 2, mouthWidth, mouthHeight,
    0, TWO_PI);                          // bottom lip

line(mouth_x, mouth_y, mouth_x + mouthWidth, mouth_y); // mouth crease

arc(leftEar_x, ear_y, earWidth, earHeight,
    PI / 2.3, PI * 1.55);                 // left ear
arc(rightEar_x, ear_y, earWidth, earHeight,
    -PI / 1.8, PI / 1.8);                 // right ear
