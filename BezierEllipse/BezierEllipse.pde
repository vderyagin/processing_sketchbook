void setup() {
  size(800, 800);
  background(255);
  translate(width / 2, height / 2);

  bezierEllipse(4, 300);
}

void bezierEllipse(int pts, float radius) {
  float
    cx1 = 0, cy1 = 0,
    cx2 = 0, cy2 = 0,
    ax = 0, ay = 0,
    rot = TWO_PI / pts,
    theta = 0,
    controlTheta1 = rot / 3.0,
    controlTheta2 = controlTheta1 * 2.0,
    controlRadius = radius / cos(controlTheta1);

  beginShape();

  for (int i = 0; i < pts; i++) {
    cx1 = cos(theta + controlTheta1) * controlRadius;
    cy1 = sin(theta + controlTheta1) * controlRadius;
    cx2 = cos(theta + controlTheta2) * controlRadius;
    cy2 = sin(theta + controlTheta2) * controlRadius;
    ax = cos(theta + rot) * radius;
    ay = sin(theta + rot) * radius;

    if (i == 0) {
      vertex(cos(0) * radius, sin(0) * radius);
    }

    if (i == pts - 1) {
      bezierVertex(cx1, cy1, cx2, cy2, cos(0) * radius, sin(0) * radius);
    } else {
      bezierVertex(cx1, cy1, cx2, cy2, ax, ay);
    }

    float cx1Next = cos(theta + controlTheta1 + rot) * controlRadius;
    float cy1Next = sin(theta + controlTheta1 + rot) * controlRadius;
    line(ax, ay, cx1Next, cy1Next);
    line(ax, ay, cx2, cy2);

    fill(0, 0, 255);
    rect(cx1 - 3, cy1 - 3, 6, 6);
    fill(0, 255, 255);
    rect(cx2 - 3, cy2 - 3, 6, 6);
    fill(255, 127, 0);
    rect(ax, ay, 6, 6);

    theta += rot;
  }

  fill(0, 127);
  noStroke();
  endShape();
}
