TTH t;

void setup() {
  size(600, 600, P3D);
  noStroke();
  t = new TTH(200);
}

void draw() {
  background(50);
  lights();
  translate(width / 2, height / 2);
  rotateX(frameCount * PI / 275);
  rotateY(frameCount * PI / 400);
  rotateZ(frameCount * PI / 175);
  t.display();
}
