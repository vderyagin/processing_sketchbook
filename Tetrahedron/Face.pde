class Face {
  PVector[] vecs = new PVector[3];

  Face(PVector v0, PVector v1, PVector v2) {
    vecs[0] = v0;
    vecs[1] = v1;
    vecs[2] = v2;
  }

  void display() {
    beginShape();
    for (PVector vec : vecs) {
      vertex(vec.x, vec.y, vec.z);
    }
    endShape();
  }
}
