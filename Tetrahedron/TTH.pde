class TTH {
  float r;

  PVector[]  verts = {
    new PVector(0, 0, 1),
    new PVector(0.945, 0, -0.333),
    new PVector(-0.461, -0.816, -0.333),
    new PVector(-0.461, 0.816, -0.333),
  };

  Tuple[] inds = {
    new Tuple(0, 1, 3),
    new Tuple(0, 3, 2),
    new Tuple(0, 2, 1),
    new Tuple(1, 2, 3),
  };

  Face[] faces = new Face[4];

  TTH(float r) {
    this.r = r;

    for (int i = 0; i < 4; i++) {
      faces[i] = new Face(verts[inds[i].elem0],
                          verts[inds[i].elem1],
                          verts[inds[i].elem2]);
    }
  }

  void display() {
    pushMatrix();
    scale(r);
    for (Face face : faces) {
      face.display();
    }
    popMatrix();
  }
}
