box[] boxes;
int
  h = 4,
  w = h,
  boxSize = 200;

void setup() {
  size(800, 800);
  smooth();

  boxes = new box[h * w];

  int boxIdx = 0;
  for (int x = 0; x < w; x++) {
    for (int y = 0; y < h; y++) {
      boxes[boxIdx++] = new box(boxSize * x, boxSize * y, boxSize, boxSize, 50);
    }
  }
}

void draw() {
  background(255);
  for (box box : boxes) {
    box.update();
    box.display();
  }
}
