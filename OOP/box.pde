public class box {
  float x, y, w, h;
  int nBalls;
  int ballRadius = 2;
  ball[] balls;

  box(float x, float y, float w, float h, int n) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.nBalls = n;
    balls = new ball[nBalls];

    for (int i = 0; i < balls.length; i++) {
      balls[i] = new ball(random(0, w), random(0, h), ballRadius, color(0), this);
    }
  }

  void display() {
    pushMatrix();
    translate(x, y);
    stroke(0);
    fill(255);
    rect(0, 0, w, h);
    for (ball ball : balls) {
      ball.display();
    }
    popMatrix();
  }

  void update() {
    for (ball ball : balls) {
      ball.move();
    }
  }
}
