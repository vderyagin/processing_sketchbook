class ball {
  color ballColor;
  box b;

  PVector
    oldLocation,
    location,
    nudge;

  float
    radius,
    gravity = 0.1;

  ball(box b) {
    this(random(b.w), random(b.h), random(10, 40),
         color(random(255), random(255), random(255)), b);
  }

  ball(float r, color c, box b) {
    this(random(b.w), random(b.h), r, c, b);
  }

  ball(float x, float y, float r, color c, box b) {
    this.location = new PVector(x, y);
    this.radius = r;
    this.ballColor = c;
    this.oldLocation = location.get();
    this.nudge = new PVector(random(1, 3), random(1, 3));
    location.add(nudge);
    this.b = b;
  }

  void move() {
    PVector temp = location.get();
    location.x += (location.x - oldLocation.x);
    location.y += (location.y - oldLocation.y);
    oldLocation.set(temp);

    bounce();
  }

  void display() {
    noStroke();
    fill(ballColor);
    ellipse(location.x, location.y, 2 * radius, 2 * radius);
  }

  void bounce() {
    if (location.x > (b.w - radius)) {
      location.x = b.w - radius;
      oldLocation.x = location.x;
      location.x -= nudge.x;
    }

    if (location.x < radius) {
      location.x = radius;
      oldLocation.x = location.x;
      location.x += nudge.x;
    }

    if (location.y > (b.h - radius)) {
      location.y = b.h - radius;
      oldLocation.y = location.y;
      location.y -= nudge.y;
    }

    if (location.y < radius) {
      location.y = radius;
      oldLocation.y = location.y;
      location.y += nudge.y;
    }
  }
}
