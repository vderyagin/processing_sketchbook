import processing.video.*;

Capture vid;
int s = 10;

void setup() {
  size(640, 480);
  vid = new Capture(this, width, height);
  vid.start();
}

void draw() {
  if (vid.available()) {
    vid.read();
    vid.loadPixels();
    for (int y = 0; y < height; y += s) {
      for (int x = 0; x < width; x += s) {
        color c = vid.pixels[y * vid.width + x];
        noStroke();
        fill(c);
        rect(x, y, s, s);
      }
    }
  }
}
