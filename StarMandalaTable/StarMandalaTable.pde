void setup() {
  size(1000, 1000);
  background(255);

  int
    rows = 4,
    cols = 4;

  float outerRadius = width / cols;

  int pointCount, steps;
  float innerRadiusFactor = 0.7;

  float
    innerRadius,
    outerRadiusRatio,
    innerRadiusRatio,
    shadeRatio,
    rotationRatio;

  translate(outerRadius / 2, outerRadius / 2);

  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      pointCount = int(random(5, 15));
      steps = int(random(3, 20));
      innerRadius = outerRadius * random(0.3, 0.9);
      outerRadiusRatio = outerRadius / steps;
      innerRadiusRatio = innerRadius / steps;
      float randCol = random(255, 255);
      shadeRatio = randCol / steps;
      rotationRatio = random(90, 200) / steps;

      pushMatrix();
      translate(outerRadius * j, outerRadius * i);
      for (int k = 0; k < steps; k++) {
        fill(shadeRatio * k);
        stroke(randCol - shadeRatio * k, 100);
        pushMatrix();
        scale(0.4);
        rotate(rotationRatio * k * PI / 180);
        star(pointCount, outerRadius - outerRadiusRatio * k, innerRadius - innerRadiusRatio * k);
        popMatrix();
      }
      popMatrix();
    }
  }
}

void star(int pointCount, float innerRadius, float outerRadius) {
  float theta = 0.0;

  int vertCount = pointCount * 2;
  float thetaRot = TWO_PI / vertCount;
  float tempRadius = 0.0;
  float x = 0.0, y = 0.0;

  beginShape();
  for (int i = 0; i < pointCount; i++) {
    for (int j = 0; j < 2; j++) {
      tempRadius = innerRadius;

      if (j % 2 == 0) {
        tempRadius = outerRadius;
      }

      x = cos(theta) * tempRadius;
      y = sin(theta) * tempRadius;
      vertex(x, y);
      theta += thetaRot;
    }
  }
  endShape(CLOSE);
}
