float ax1, ay1, ax2, ay2, cx1, cy1, cx2, cy2;
boolean isOnControl1, isOnControl2, isOnAnchor1, isOnAnchor2;
float radius = 5;

void setup() {
  size(600, 600);

  cx1 = random(100, width - 100);
  cy1 = random(100, height - 100);
  cx2 = random(100, width - 100);
  cy2 = random(100, height - 100);
  ax1 = random(100, width - 100);
  ay1 = random(100, height - 100);
  ax2 = random(100, width - 100);
  ay2 = random(100, height - 100);
}

void draw() {
  background(255);
  noFill();
  strokeWeight(4);
  stroke(0);

  // curve
  beginShape();
  vertex(ax2, ay2);
  bezierVertex(cx1, cy1, cx2, cy2, ax1, ay1);
  endShape();

  // center point
  fill(200);
  strokeWeight(1);
  ellipse(ax2, ay2, radius * 2, radius * 2);

  // connecting handles
  line(cx1, cy1, ax2, ay2);
  line(cx2, cy2, ax1, ay1);

  // control points
  fill(0, 0, 255);
  rect(cx1 - radius, cy1 - radius, radius * 2, radius * 2);
  rect(cx2 - radius, cy2 - radius, radius * 2, radius * 2);

  // anchor point
  fill(255, 127, 0);
  ellipse(ax1, ay1, radius * 2, radius * 2);

  // mouse position detection
  if (dist(mouseX, mouseY, ax1, ay1) < radius) {
    isOnAnchor1 = true;
  } else if (dist(mouseX, mouseY, ax2, ay2) < radius) {
    isOnAnchor2 = true;
  } else if (dist(mouseX, mouseY, cx1, cy1) < radius) {
    isOnControl1 = true;
  } else if (dist(mouseX, mouseY, cx2, cy2) < radius) {
    isOnControl2 = true;
  } else {
    isOnAnchor1 = isOnAnchor2 = isOnControl1 = isOnControl2 = false;
  }
}

void mouseDragged() {
  if (isOnControl1) {
    cx1 = mouseX;
    cy1 = mouseY;
  } else if (isOnControl2) {
    cx2 = mouseX;
    cy2 = mouseY;
  } else if (isOnAnchor1) {
    ax1 = mouseX;
    ay1 = mouseY;
  } else if (isOnAnchor2) {
    ax2 = mouseX;
    ay2 = mouseY;
  }
}
