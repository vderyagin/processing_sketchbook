float ax, ay, cx, cy, bx, by;
boolean isOnControl, isOnAnchorA, isOnAnchorB;
float radius = 5;

void setup() {
  size(600, 600);

  cx = random(100, width - 100);
  cy = random(100, height - 100);
  ax = random(100, width - 100);
  ay = random(100, height - 100);
  bx = random(100, width - 100);
  by = random(100, height - 100);
}


void draw() {
  background(255);
  noFill();
  strokeWeight(4);
  stroke(0);

  beginShape();
  vertex(bx, by);
  quadraticVertex(cx, cy, ax, ay);
  endShape();

  fill(200);
  strokeWeight(1);
  ellipse(bx, by, radius * 2, radius * 2);

  line(cx, cy, ax, ay);
  line(cx, cy, bx, by);

  fill(0, 0, 255);
  rect(cx - radius, cy - radius, radius * 2, radius * 2);

  fill(255, 127, 0);
  ellipse(ax, ay, radius * 2, radius * 2);

  if (dist(mouseX, mouseY, ax, ay) < radius) {
    isOnAnchorA = true;
  } else if (dist (mouseX, mouseY, bx, by) < radius) {
    isOnAnchorB = true;
  } else if (dist (mouseX, mouseY, cx, cy) < radius) {
    isOnControl = true;
  } else {
    isOnAnchorA = isOnAnchorB = isOnControl = false;
  }
}

void mouseDragged() {
  if (isOnControl) {
    cx = mouseX;
    cy = mouseY;
  } else if (isOnAnchorA) {
    ax = mouseX;
    ay = mouseY;
  } else if (isOnAnchorB) {
    bx = mouseX;
    by = mouseY;
  }
}
